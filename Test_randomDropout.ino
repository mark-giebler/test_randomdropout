/**
 * A random circuit disconnect using mechanical relay.
 * Used to simulate communication drop out between nodes.
 * Mark Giebler
 * 2020-06-20
 * 
 * Relay connected to D13
 *  High - relay activated ==> drop out simulated
 * 
 * Random time with relay deactivated (0.8 to 9.5 Seconds )
 * Random number of relay activations (2 to 6)
 * Random duratin of relay activations (20 to 200 mSec)
 * 
 * Hardware:
 *  Seeedstudio Seeeduino  "Arduino Uno"
 *  Home brew relay break-out board.
 *
 * Arduino IDE version: 1.8.9
 *
 * Notes:
 *
 */

#define LED_STATUS  (13)

#define OP_MODE     (4)     // High is original mode. Low more drops in a row, longer time between drops.
#define OP_MODE_DN  (3)     // Pull down for op mode jumper.

#define RANGE_PULSE (6)     // High is original min/max pulse range, Low is wider pulse range.
#define RANGE_PULSE_DN  (5) // Pull down for short pulse jumper

#define MIN_REST  (800ul)   // mSec  min rest between drop outs
#define MAX_REST  (9500ul)  // mSec  max rest between drop outs
// jumper option for longer rest
#define MAX_REST_OP  (12000ul)  // mSec  max rest between drop outs

#define MIN_PULSE   (20ul)  // mSec  min pulse width of frop outs
#define MAX_PULSE   (200ul) // mSec  max pulse width of drop outs
#define MAX_PULSES  (8ul)   // max number of pulses in a row

// Jumper option for wider pulse range
#define MIN_PULSE_W   (8ul)  // mSec  min pulse width of frop outs
#define MAX_PULSE_W   (400ul) // mSec  max pulse width of drop outs
#define MAX_PULSES_OP  (13ul)   // max number of pulses in a row 

long randNumber;
long randRest;
long randPulse;
long randPulseCount;

void setup() {
	pinMode(LED_STATUS, OUTPUT);
	digitalWrite(LED_STATUS, LOW);  // start with LED and Relay off

	// mode select IO for more drops in a row, longer pause.
	pinMode(OP_MODE_DN, OUTPUT);
	digitalWrite(OP_MODE_DN, LOW);
	pinMode(OP_MODE, INPUT_PULLUP);

	// min max pulse range select
	pinMode(RANGE_PULSE_DN, OUTPUT);
	digitalWrite(RANGE_PULSE_DN, LOW);
	pinMode(RANGE_PULSE, INPUT_PULLUP);

	Serial.begin(115200);
	// if analog input pin 0 is floating, random analog
	// noise will cause the call to analogRead() to generate
	// different seed numbers each time the sketch runs.
	uint32_t seed = (analogRead(0)*13)+39;
	randomSeed(seed);

	Serial.println("");
	Serial.println("Mark's random comm dropper v1.02");
	Serial.print("Seed: "); Serial.println(seed);
	Serial.print("Jumper pins: "); 
	Serial.print(OP_MODE); Serial.print(" "); Serial.print(OP_MODE_DN); 
	Serial.println(" for longer rest time and more pulses in a row (OP_MODE)");
	Serial.print("Jumper pins: "); 
	Serial.print(RANGE_PULSE); Serial.print(" "); Serial.print(RANGE_PULSE_DN); 
	Serial.println(" for wider pulse width range (RANGE_PULSE)");
	// report jumper settings
	if(digitalRead(OP_MODE))
	{
		Serial.println("Original OP_MODE");
	}else
	{
		Serial.println("New OP_MODE");
	}
	if(digitalRead(RANGE_PULSE))
	{
		Serial.println("Original RANGE_PULSE");
	}else
	{
		Serial.println("New RANGE_PULSE");
	}
	// Delay start up.
	delay(2000);
}

void loop() {
	// random rest period (no drop out  time)
	uint32_t min_rest = MIN_REST;
	uint32_t max_rest = MAX_REST+1ul;
	if(!digitalRead(OP_MODE))
	{
		max_rest = MAX_REST_OP + 1ul;
	}
	randRest = random(min_rest, max_rest);
	Serial.print("Rest mSec: ");
	Serial.println(randRest);
	delay(randRest);
	
	// random drop out pulse width
	uint32_t min_pulse = MIN_PULSE;
	uint32_t max_pulse = MAX_PULSE+1ul;
	if(!digitalRead(RANGE_PULSE) )
	{
		min_pulse = MIN_PULSE_W;
		max_pulse = MAX_PULSE_W+1ul;
	}
	Serial.print("Pulse width mSec: ");
	randPulse = random(min_pulse, max_pulse);
	Serial.println(randPulse);

	// random drop out repeat count
	uint32_t min_pulses = 2ul;
	uint32_t max_pulses = MAX_PULSES+1;
	if(!digitalRead(OP_MODE) )
	{
		max_pulses = MAX_PULSES_OP+1;
	}
	
	Serial.print("Pulse repeat count: ");
	randPulseCount = random( min_pulses, max_pulses);
	Serial.println(randPulseCount);
	
	while(randPulseCount--)
	{
		// pulse the relay on (Cause drop out)
		digitalWrite(LED_STATUS, HIGH);
		delay(randPulse);
		digitalWrite(LED_STATUS, LOW);
		delay(randPulse+5ul);
	}
}
